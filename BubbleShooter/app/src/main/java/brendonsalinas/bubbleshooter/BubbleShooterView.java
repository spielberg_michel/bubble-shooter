package brendonsalinas.bubbleshooter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;
import java.lang.Math;

/**
 * Created by brendonsalinas on 4/8/15.
 */
// This is the ‘‘ game engine ’ ’.
public class BubbleShooterView extends SurfaceView implements SurfaceHolder. Callback {
    BubbleShooterThread bst;
    int screenWidth, screenHeight, screenR, restart = 0;
    boolean Up = false;
    int[] Colors;
    int moveX, moveY, count = 0, level=0;
    double angle, newX, newY;
    Bubble[][] Bubbles;
    Bubble MovingBubble;

    public BubbleShooterView ( Context context ) {
        super ( context ) ;
        // Notify the SurfaceHolder that you ’d like to receive
        // SurfaceHolder callbacks .
        getHolder () . addCallback(this) ;
        setFocusable ( true );
        // Initialize game state variables . DON ’T RENDER THE GAME YET .
        Bubbles = new Bubble[16][];
        MovingBubble = new Bubble(this);
    }
    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        // Construct game initial state ( bubbles , etc .)

        screenWidth = holder.getSurfaceFrame().right;
        screenHeight = holder.getSurfaceFrame().bottom;
        screenR = screenWidth/20;
        

        //Create an array of colors in order to get a random color for each bubble
        Colors = new int[8];
        Colors[0] = Color.BLACK;
        Colors[1] = Color.GREEN;
        Colors[2] = Color.BLUE;
        Colors[3] = Color.MAGENTA;
        Colors[4] = Color.RED;
        Colors[5] = Color.GRAY;
        Colors[6] = Color.YELLOW;
        Colors[7] = Color.CYAN;



        //Create an array of bubbles that could potentially fill the screen
        for (int i = 0; i<16; i++) {
            Bubbles[i] = new Bubble[10];
            Bubbles[i+1] = new Bubble[9];
            i++;
        }
        reset();

        // Launch animator thread .
        bst = new BubbleShooterThread(this);
        bst.start() ;
    }
    @Override
    public void surfaceChanged ( SurfaceHolder holder, int format , int width , int height ) {
    // Respond to surface changes , e . g . , aspect ratio changes .
    }
    @Override
    public void surfaceDestroyed ( SurfaceHolder holder ) {
    // The cleanest way to stop a thread is by interrupting it .
    // BubbleShooterThread regularly checks its interrupt flag .
        bst.interrupt() ;
    }
    @Override
    public void onDraw ( Canvas c ) {
        super . onDraw ( c ) ;
        renderGame ( c ) ;
    }
    @Override
    public boolean onTouchEvent ( MotionEvent e ) {
    // Update game state in response to events :
    // touch - down , touch - up , and touch - move .
    // Current finger position .

        int curX = (int) e.getX();
        int curY = (int) e.getY();



        switch ( e . getAction () ) {
            case MotionEvent . ACTION_DOWN :
                // Update Game State .
                break ;
            case MotionEvent . ACTION_MOVE :
                // Update Game State .
                break ;
            case MotionEvent . ACTION_UP :
                //Getting the x and y for the angle of movement


                if(!Up)
                {
                    newX = (double) (curX - (screenWidth/2));
                    newY = (double) ((screenHeight) - curY);
                    angle = Math.atan2(newY, newX);
                    moveX = (int) Math.ceil((screenR) * Math.cos(angle));
                    moveY = (int) -Math.ceil((screenR) * Math.sin(angle));
                    Up = true;
                }

                //Setup up level
                if(level==0)
                {
                    Up = false;
                    //Level 4
                    if(curX<=(screenWidth/3))
                    {
                        level = 4;
                    }
                    //Level 6
                    else if((curX<=((screenWidth/3)*2)) &&(moveX>=0))
                    {
                        level = 6;
                    }
                    //Level 8
                    else if(curX<=(screenWidth)&& (curX>=((screenWidth/3)*2)))
                    {
                        level = 8;
                    }
                    reset();
                }
                // Update Game State .
                break ;
        }

        return true ;
    }
    public void advanceFrame ( Canvas c ) {
    // Update game state to animate moving or exploding bubbles
    // ( e . g . , advance location of moving bubble ) .
        //Actually changing the location of the bubble that the user shoots

        if (Up) {
            MovingBubble.bX+=moveX;
            MovingBubble.bY+=moveY;
            if (MovingBubble.bX > (screenWidth-screenR)) {
                moveX = -moveX;
                MovingBubble.bX = screenWidth-screenR;
            }
            else if (MovingBubble.bX < screenR) {
                moveX = -moveX;
                MovingBubble.bX = screenR;
            }
            checkBubble();
        }
        renderGame ( c ) ;
    }
    private void renderGame ( Canvas c ) {
    // Render the game elements : bubbles ( fixed , moving , exploding )
    // and aiming arrow .
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        c.drawPaint(paint);
        MovingBubble.draw(c);

        //Will delete bubbles that have no bubbles in the back row
        for (int i = 0; i<16; i++)
        {
            for (int j = 0; j < Bubbles[i].length; j++)
            {
                //Starts after row zero
                if(i>=1)
                {
                    //If the column is zero and odd row
                    if(j==0 && (i%2==1))
                    {

                        //Check top right  AND Check top left
                        if ((!Bubbles[i-1][j+1].filled) && (!Bubbles[i-1][j].filled))
                        {
                            Bubbles[i][j].filled = false;
                        }
                    }

                    //The column is zero and even row
                    else if(j==0 && (i%2==0))
                    {
                        //Check top right
                        if ((!Bubbles[i-1][j].filled))
                        {
                            Bubbles[i][j].filled = false;
                        }
                    }

                    //the end of an odd row
                    else if((i%2==1))
                    {
                        //Check the top left and top right
                        if ((!Bubbles[i-1][j+1].filled) && (!Bubbles[i-1][j].filled))
                        {
                            Bubbles[i][j].filled = false;
                        }
                    }

                    //The end of an even row
                    else if( (j==9) && (i%2==0))
                    {
                        //Check the top left
                        if ((!Bubbles[i-1][j-1].filled))
                        {
                            Bubbles[i][j].filled = false;
                        }
                    }

                    //An even row
                    else if( i%2==0 )
                    {
                        //Check top left and top right
                        if ((!Bubbles[i-1][j].filled) && (!Bubbles[i-1][j-1].filled))
                        {
                            Bubbles[i][j].filled = false;
                        }
                    }

                }



            }
        }
        for (int i = 0; i<16; i++) {
            //Let each object render itself to the canvas.
            //This way, we can add more object types later without // having to modify this loop.
            for (int j = 0; j<Bubbles[i].length; j++) {
                if(Bubbles[i][j].filled)
                {
                    Bubbles[i][j].draw(c);
                }
            }
        }


        for (int i = 0; i<8; i++) {
            if (Bubbles[14][i].filled) {
                reset();
                break;
            }
        }

        if (restart == 0) {reset();}
    }

    private void reset() {
        int x, y = screenR;
        restart = 0;
        //Randomly create bubbles that fill up the first 7 rows
        for (int i = 0; i<16; i++) {
            Random r = new Random();
            if (i%2 == 0) {
                x = screenR;
            }
            else {
                x = 2*screenR;
            }

            for (int j = 0; j<Bubbles[i].length; j++) {
                Bubbles[i][j] = new Bubble(this);
                if (i<7) {
                    int ranC = r.nextInt(8);
                    Bubbles[i][j].bX = x;
                    Bubbles[i][j].bY = y;
                    //Level 1 has four colors
                    if(level==4)
                    {
                        Bubbles[i][j].bColor = Colors[ranC%4];
                    }
                    //Level 2 has 6 colors
                    else if(level==6)
                    {
                        Bubbles[i][j].bColor = Colors[ranC%6];
                    }
                    //Level 3 has 8 colors
                    else if(level==8)
                    {
                        Bubbles[i][j].bColor = Colors[ranC%8];
                    }

                    //Level 0, sample set of bubbles while user selects level
                    else if(level==0)
                    {
                        Bubbles[i][j].bColor = Colors[ranC];
                    }
                    Bubbles[i][j].filled = true;
                    Bubbles[i][j].bRadius = screenR;
                    restart++;
                }

                x += 2*screenR;
            }
            y+=2*screenR;
        }

        //Random start bubble at the bottom/ middle of the screen
        Random r = new Random();
        int ranC = r.nextInt(8);
        MovingBubble.bX = screenWidth/2;
        MovingBubble.bY = screenHeight - screenR;
        if(level==0)
        {
            MovingBubble.bColor = Colors[ranC];
        }
        else
        {
            MovingBubble.bColor = Colors[ranC%level];
        }
        MovingBubble.bRadius = screenR;

    }

    private void deleteBubble(Bubble newBubble, int recursive)
    {
        int i, row, column, check, current=0;
        int [] list;
        Bubble [] obj;
        //Checks if any bubbles with the same color are near the moving bubble
        // If there are it pops the bubbles
        list = new int[6];

        obj = new Bubble [6];

        for(i=0;i<6;i++)
        {
            list[i] = 0;
        }

        //Calculate the row of the moving bubble
        row = ((newBubble.bY)/(2*screenR));

        if(row<0)
        {
            row = 0;
        }

        //Calculates the column
        if (row%2==0)
        {
            column = ((newBubble.bX - screenR) / (2*screenR));

            if (column >= 10) {
                column = 9;
            }
            else if (column < 0) {
                column = 0;
            }
        }
        else {
            column = ((newBubble.bX - (2*screenR)) / (2*screenR));

            if (column >= 9) {
                column = 8;
            }
            else if (column < 0) {
                column = 0;
            }
        }

        //Starts looking for bubbles with the same color
        //If bubble is in column zero
        if(column == 0)
        {
            //If the row is zero
            if(row==0)
            {
                //Check the right of the bubble
                if(Bubbles[row][column+1].filled && Bubbles[row][column+1].marked &&
                    Bubbles[row][column+1].bColor==newBubble.bColor)
                {
                    obj[3] = Bubbles[row][column+1];
                    list[3] = 1;
                    Bubbles[row][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom right of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column];
                    list[5] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }
            }

            //the bubble is in an odd row
            else if(row%2 == 1)
            {
                //Check the top left
                if(Bubbles[row-1][column].filled && Bubbles[row-1][column].marked &&
                    Bubbles[row-1][column].bColor==newBubble.bColor)
                {
                    obj[0] = Bubbles[row-1][column];
                    list[0]=1;
                    Bubbles[row-1][column].marked = false;
                    count++;
                    current++;
                }

                //Check the top right bubble
                if(Bubbles[row-1][column+1].filled && Bubbles[row-1][column+1].marked &&
                    Bubbles[row-1][column+1].bColor==newBubble.bColor)
                {
                    obj[1] = Bubbles[row-1][column+1];
                    list[1] = 1;
                    Bubbles[row-1][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the right of the bubble
                if(Bubbles[row][column+1].filled && Bubbles[row][column+1].marked &&
                    Bubbles[row][column+1].bColor==newBubble.bColor)
                {
                    obj[3] = Bubbles[row][column+1];
                    list[3] = 1;
                    Bubbles[row][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom left of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[4] = Bubbles[row+1][column];
                    list[4] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom right of the bubble
                if(Bubbles[row+1][column+1].filled && Bubbles[row+1][column+1].marked &&
                    Bubbles[row+1][column+1].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column+1];
                    list[5] = 1;
                    Bubbles[row+1][column+1].marked = false;
                    count++;
                    current++;
                }
            }
            //the bubble is in an even row
            else
            {
                //Check the top right bubble
                if(Bubbles[row-1][column].filled && Bubbles[row-1][column].marked &&
                    Bubbles[row-1][column].bColor==newBubble.bColor)
                {
                    obj[1] = Bubbles[row-1][column];
                    list[1] = 1;
                    Bubbles[row-1][column].marked = false;
                    count++;
                    current++;
                }

                //Check the right of the bubble
                if(Bubbles[row][column+1].filled && Bubbles[row][column+1].marked &&
                    Bubbles[row][column+1].bColor==newBubble.bColor)
                {
                    obj[3] = Bubbles[row][column+1];
                    list[3] = 1;
                    Bubbles[row][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom right of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column];
                    list[5] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }
            }
        }

        //If the bubble is in row zero
        else if(row == 0)
        {
            //The bubble is in column 9
            if(column == 9)
            {
                //Check the left of the bubble
                if(Bubbles[row][column-1].filled && Bubbles[row][column-1].marked &&
                    Bubbles[row][column-1].bColor==newBubble.bColor)
                {
                    obj[2] = Bubbles[row][column-1];
                    list[2] = 1;
                    Bubbles[row][column-1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom left of the bubble
                if(Bubbles[row+1][column-1].filled && Bubbles[row+1][column-1].marked &&
                    Bubbles[row+1][column-1].bColor==newBubble.bColor)
                {
                    obj[4] = Bubbles[row+1][column-1];
                    list[4] = 1;
                    Bubbles[row+1][column-1].marked = false;
                    count++;
                    current++;
                }
            }

            //The bubble is not in column 9
            else
            {
                //Check the left of the bubble
                if(Bubbles[row][column-1].filled && Bubbles[row][column-1].marked &&
                    Bubbles[row][column-1].bColor==newBubble.bColor)
                {
                    obj[2] = Bubbles[row][column-1];
                    list[2] = 1;
                    Bubbles[row][column-1].marked = false;
                    count++;
                    current++;
                }

                //Check the right of the bubble
                if(Bubbles[row][column+1].filled && Bubbles[row][column+1].marked &&
                    Bubbles[row][column+1].bColor==newBubble.bColor)
                {
                    obj[3] = Bubbles[row][column+1];
                    list[3] = 1;
                    Bubbles[row][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom left of the bubble
                if(Bubbles[row+1][column-1].filled && Bubbles[row+1][column-1].marked &&
                    Bubbles[row+1][column-1].bColor==newBubble.bColor)
                {
                    obj[4] = Bubbles[row+1][column-1];
                    list[4] = 1;
                    Bubbles[row+1][column-1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom right of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column];
                    list[5] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }
            }

        }

        //If the bubble is in the last column
        //if the last column is 8
        else if((row%2 == 1) && (column == 8))
        {
            //Check the top left bubble
            if(Bubbles[row-1][column].filled && Bubbles[row-1][column].marked &&
                Bubbles[row-1][column].bColor==newBubble.bColor)
            {
                obj[0] = Bubbles[row-1][column];
                list[0]=1;
                Bubbles[row-1][column].marked = false;
                count++;
                current++;
            }

            //Check the top right bubble
            if(Bubbles[row-1][column+1].filled && Bubbles[row-1][column+1].marked &&
                Bubbles[row-1][column+1].bColor==newBubble.bColor)
            {
                obj[1] = Bubbles[row-1][column+1];
                list[1] = 1;
                Bubbles[row-1][column+1].marked = false;
                count++;
                current++;
            }

            //Check the left of the bubble
            if(Bubbles[row][column-1].filled && Bubbles[row][column-1].marked &&
                Bubbles[row][column-1].bColor==newBubble.bColor)
            {
                obj[2] = Bubbles[row][column-1];
                list[2] = 1;
                Bubbles[row][column-1].marked = false;
                count++;
                current++;
            }

            //Check the bottom left of the bubble
            if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                Bubbles[row+1][column].bColor==newBubble.bColor)
            {
                obj[4] = Bubbles[row+1][column];
                list[4] = 1;
                Bubbles[row+1][column].marked = false;
                count++;
                current++;
            }

            //Check the bottom right of the bubble
            if(Bubbles[row+1][column+1].filled && Bubbles[row+1][column+1].marked &&
                Bubbles[row+1][column+1].bColor==newBubble.bColor)
            {
                obj[5] = Bubbles[row+1][column+1];
                list[5] = 1;
                Bubbles[row+1][column+1].marked = false;
                count++;
                current++;
            }
        }

        //if the last column is 9
        else if((row%2 == 0) && (column == 9))
        {
            //check the bubble to the top left
            if(Bubbles[row-1][column-1].filled && Bubbles[row-1][column-1].marked &&
                Bubbles[row-1][column-1].bColor==newBubble.bColor)
            {
                obj[0] = Bubbles[row-1][column-1];
                list[0]=1;
                Bubbles[row-1][column-1].marked = false;
                count++;
                current++;
            }

            //Check the left of the bubble
            if(Bubbles[row][column-1].filled && Bubbles[row][column-1].marked &&
                Bubbles[row][column-1].bColor==newBubble.bColor)
            {
                obj[2] = Bubbles[row][column-1];
                list[2] = 1;
                Bubbles[row][column-1].marked = false;
                count++;
                current++;
            }

            //Check the bottom left of the bubble
            if(Bubbles[row+1][column-1].filled && Bubbles[row+1][column-1].marked &&
                Bubbles[row+1][column-1].bColor==newBubble.bColor)
            {
                obj[4] = Bubbles[row+1][column-1];
                list[4] = 1;
                Bubbles[row+1][column-1].marked = false;
                count++;
                current++;
            }
        }

        //If bubble is in the center of the screen
        else
        {
            //Checks if the row is even or odd
            //If the row is even
            if(row%2 == 0)
            {
                //check the bubble to the top left
                if(Bubbles[row-1][column-1].filled && Bubbles[row-1][column-1].marked &&
                    Bubbles[row-1][column-1].bColor==newBubble.bColor)
                {
                    obj[0] = Bubbles[row-1][column-1];
                    list[0]=1;
                    Bubbles[row-1][column-1].marked = false;
                    count++;
                    current++;
                }

                //Check the top right bubble
                if(Bubbles[row-1][column].filled && Bubbles[row-1][column].marked &&
                    Bubbles[row-1][column].bColor==newBubble.bColor)
                {
                    obj[1] = Bubbles[row-1][column];
                    list[1] = 1;
                    Bubbles[row-1][column].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom left of the bubble
                if(Bubbles[row+1][column-1].filled && Bubbles[row+1][column-1].marked &&
                    Bubbles[row+1][column-1].bColor==newBubble.bColor)
                {
                    obj[4] = Bubbles[row+1][column-1];
                    list[4] = 1;
                    Bubbles[row+1][column-1].marked = false;
                    count++;
                    current++;
                }


                //Check the bottom right of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column];
                    list[5] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }

            }

            //The row is odd
            else
            {
                //Check the top left bubble
                if(Bubbles[row-1][column].filled && Bubbles[row-1][column].marked &&
                    Bubbles[row-1][column].bColor==newBubble.bColor)
                {
                    obj[0] = Bubbles[row-1][column];
                    list[0]=1;
                    Bubbles[row-1][column].marked = false;
                    count++;
                    current++;
                }

                //Check the top right bubble
                if(Bubbles[row-1][column+1].filled && Bubbles[row-1][column+1].marked &&
                    Bubbles[row-1][column+1].bColor==newBubble.bColor)
                {
                    obj[1] = Bubbles[row-1][column+1];
                    list[1] = 1;
                    Bubbles[row-1][column+1].marked = false;
                    count++;
                    current++;
                }

                //Check the bottom left of the bubble
                if(Bubbles[row+1][column].filled && Bubbles[row+1][column].marked &&
                    Bubbles[row+1][column].bColor==newBubble.bColor)
                {
                    obj[4] = Bubbles[row+1][column];
                    list[4] = 1;
                    Bubbles[row+1][column].marked = false;
                    count++;
                    current++;
                }


                //Check the bottom right of the bubble
                if(Bubbles[row+1][column+1].filled && Bubbles[row+1][column+1].marked &&
                    Bubbles[row+1][column+1].bColor==newBubble.bColor)
                {
                    obj[5] = Bubbles[row+1][column+1];
                    list[5] = 1;
                    Bubbles[row+1][column+1].marked = false;
                    count++;
                    current++;
                }
            }

            //Check the left of the bubble
            if(Bubbles[row][column-1].filled && Bubbles[row][column-1].marked &&
                Bubbles[row][column-1].bColor==newBubble.bColor)
            {
                obj[2] = Bubbles[row][column-1];
                list[2] = 1;
                Bubbles[row][column-1].marked = false;
                count++;
                current++;
            }

            //Check the right of the bubble
            if(Bubbles[row][column+1].filled && Bubbles[row][column+1].marked &&
                Bubbles[row][column+1].bColor==newBubble.bColor)
            {
                obj[3] = Bubbles[row][column+1];
                list[3] = 1;
                Bubbles[row][column+1].marked = false;
                count++;
                current++;
            }
        }

        check=1;

        if(recursive==0)
        {
            if(current>=1)
            {
                for(i=0;i<6;i++)
                {
                    if(list[i] == 1)
                    {
                        deleteBubble(obj[i], check);
                    }


                }

            }
        }
        //The function was called recursively
        else if(recursive == 1)
        {
            if(current>=1)
            {

                for(i=0;i<6;i++)
                {
                    if(list[i] == 1)
                    {
                        deleteBubble(obj[i], check);
                    }
                }

            }
        }
    }

    private void checkBubble() {
        int x, i, j=0, row, column, newColor, recursive=0;
        //Checks if any bubbles are near the moving bubble
        // If there are it stops the bubble where it is

        row = ((MovingBubble.bY)/(2*screenR));

        if(row<0)
        {
            row = 0;
        }
        //Checks the next row of bubbles
        if(row>=0)
        {
            if (row%2==0) {
                column = ((MovingBubble.bX - screenR) / (2*screenR));

                if (column >= 10) {
                    column = 9;
                }
                else if (column < 0) {
                    column = 0;
                }
             }
            else {
                column = ((MovingBubble.bX - (2*screenR)) / (2*screenR));

                if (column >= 9) {
                    column = 8;
                }
                else if (column < 0) {
                    column = 0;
                }
            }

            //Checks if bubble is in row zero

            if(row ==0 )
            {
                j= 1;
            }
            //Checks if column is zero
            else if(column == 0 )
            {
                //Check the right
                if(Bubbles[row][column+1].filled && (moveX>0))
                {
                    j=1;
                }

                //checks if row is even
                if(row%2==0)
                {
                    //Check top right
                    if(Bubbles[row-1][column].filled)
                    {
                        j = 1;
                    }
                }

                //Else row is odd
                else
                {
                    //Check top left
                    if(Bubbles[row-1][column].filled)
                    {
                        j = 1;
                    }

                    //Check top right
                    if(Bubbles[row-1][column+1].filled)
                    {
                        j = 1;
                    }
                }
            }

            //check if last column is 8
            else if(column == 8 && row%2==1)
            {
                //Check the left
                if(Bubbles[row][column-1].filled && (moveX<0) )
                {
                    j = 1;
                }

                //Check top left
                if(Bubbles[row-1][column].filled)
                {
                    j = 1;
                }

                //Check top right
                if(Bubbles[row-1][column+1].filled)
                {
                    j = 1;
                }
            }

            //Else the last column is 9
            else if (column == 9)
            {

                //Check the left
                if(Bubbles[row][column-1].filled && (moveX<0))
                {
                    j = 1;
                }

                //Check top left
                if(Bubbles[row-1][column-1].filled)
                {
                    j = 1;
                }
            }

            //the row is even
            else if(row%2 == 0)
            {
                //Check top left
                if(Bubbles[row-1][column-1].filled)
                {
                    j = 1;
                }

                //Check top right
                else if(Bubbles[row-1][column].filled)
                {
                    j = 1;
                }

                //Check the left
                else if(Bubbles[row][column-1].filled && (moveX<0))
                {
                    j = 1;
                }

                //Check the right
                else if(Bubbles[row][column+1].filled && (moveX>0))
                {
                    j=1;
                }


            }
            //The row is odd
            else if(row%2 ==1)
            {
                //Check the left
                if(Bubbles[row][column-1].filled && (moveX<-5))
                {
                    j = 1;
                }

                //Check the right
                if(Bubbles[row][column+1].filled && (moveX>0))
                {
                    j=1;
                }

                //Check top left
                if(Bubbles[row-1][column].filled)
                {
                    j = 1;
                }

                //Check top right
                if(Bubbles[row-1][column+1].filled)
                {
                    j = 1;
                }
            }

            //If j==1 then the bubble needs to stop
            if(j == 1)
            {
                Up = false;
                if((row)%2==0)
                {
                    x = ((2*screenR) * (column))+(screenR);
                }
                else
                {
                    x = ((2*screenR) * (column)) + (2*screenR);
                }

                newColor = MovingBubble.bColor;

                Bubbles[row][column].bX = x;
                Bubbles[row][column].bY = ((row) * (2*screenR)) + (screenR);
                Bubbles[row][column].bColor = newColor;
                Bubbles[row][column].filled = true;
                Bubbles[row][column].bRadius = screenR;
                MovingBubble.marked = true;
                restart++;
                count = 0;
                deleteBubble(MovingBubble, recursive);

                if ((count)>=3) {
                    for (i = 0; i<16; i++) {
                        for (j = 0; j<Bubbles[i].length; j++) {
                            if (!Bubbles[i][j].marked) {
                                Bubbles[i][j].filled = false;
                                Bubbles[i][j].forreal = true;
                                Bubbles[i][j].marked = true;
                                restart--;
                            }
                        }
                    }
                }
                //automatically return the array to normal
                else
                {
                    for (i = 0; i < 16; i++) {
                        for (j = 0; j < Bubbles[i].length; j++) {
                            Bubbles[i][j].marked = true;
                            Bubbles[i][j].forreal = true;
                        }
                    }
                }

                Random r = new Random();
                int ranC = r.nextInt(8);
                MovingBubble = new Bubble(this);
                MovingBubble.bX = screenWidth/2;
                MovingBubble.bY = screenHeight - screenR;
                MovingBubble.bColor = Colors[ranC%level];

            }
        }
    }
}
