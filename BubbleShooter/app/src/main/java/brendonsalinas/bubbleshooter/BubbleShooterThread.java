package brendonsalinas.bubbleshooter;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by brendonsalinas on 4/8/15.
 */
public class BubbleShooterThread extends Thread {
    BubbleShooterView bsv ;
    public BubbleShooterThread ( BubbleShooterView bsv ) {
        this . bsv = bsv ;
    }

    public void run () {
        SurfaceHolder sh = bsv.getHolder() ;
        // Main game loop .
        while ( ! Thread . interrupted () ) {
            Canvas c = sh . lockCanvas ( null ) ;
            try {
                synchronized ( sh ) {
                    bsv . advanceFrame ( c ) ;
                }
            } catch ( Exception e ) {
            } finally {
                if ( c != null ) {
                    sh . unlockCanvasAndPost ( c ) ;
                }
            }
            // Set the frame rate by setting this delay
            try {
                Thread . sleep (16) ;
            } catch ( InterruptedException e ) {
            // Thread was interrupted while sleeping .
                return ;
            }
        }
    }
}