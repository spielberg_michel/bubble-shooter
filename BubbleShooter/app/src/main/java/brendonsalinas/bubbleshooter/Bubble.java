package brendonsalinas.bubbleshooter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by brendonsalinas on 4/8/15.
 */

public class Bubble{
    //Class properties
    private static final int COLOR = Color.WHITE;
    private static final boolean STATUS = false;

    //Object properties
    private final BubbleShooterView bsv;
    public int bColor;
    public int bRadius;
    public boolean filled;

    //This field is a flag that checks whether a bubble is setup to be deleted
    //TRUE means the bubble is not going to be deleted. Deleted bubbles are False
    public boolean marked = true;
    public boolean forreal = true;
    public int bX;
    public int bY;

    public Bubble( BubbleShooterView bsv){
        this.bsv = bsv;
        this.bColor = COLOR;
        this.bRadius = bsv.getWidth()/20;
        this.filled = STATUS;
        this.bX = bRadius;
        this.bY = bRadius;
    }

    public int getX() { return bX;};
    public int getY() { return bY;};
    public int getR() { return bRadius;};

    public void draw(Canvas c) {
        Paint paint = new Paint();
        paint.setColor(bColor);
        c.drawCircle( this.getX(), this.getY(), this.getR(), paint);
    }
}
